Ext.define('T.model.hello.Client', {
    extend: 'Ext.data.Model',
    fields: ["id", "firstName", "lastName", "personalNumber", {name: "dob"}, "address", "image"],
    proxy: {
        type: 'rest',
        url: '/client',
        writer: {
            writeAllFields: true,
            writeRecordId: false
        }
    },

    validators: [
        {field: 'firstName', type: 'presence', message: 'First Name empty'},
        {field: 'lastName', type: 'presence', message: 'Last Name empty'},
        {field: 'personalNumber', type: 'presence', message: 'Personal Number empty'},
        {field: 'address', type: 'presence', message: 'Address empty'}

    ],
})