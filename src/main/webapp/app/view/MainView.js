Ext.define('T.view.MainView', {
    extend: 'Ext.tab.Panel',

    controller: {
        xclass: 'T.view.hello.Controller'
    },
    items: [{
        xtype: 'panel',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        title: 'კლიენტები',
        iconCls: 'fa fa-users',
        closable: false,
        items: [{
            xclass: 'T.view.hello.Form',
            reference: 'form'
        }, {
            xclass: 'T.view.hello.Grid',
            flex: 1,
            reference: 'grid'
        },]
    }]
});

