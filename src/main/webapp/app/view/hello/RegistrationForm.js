Ext.define('T.view.hello.RegistrationForm', {
    extend: 'Ext.form.Panel',
    layout: 'hbox',
    border: false,
    bodyPadding: 5,
    fieldDefaults: {
        anchor: '100%',
        labelWidth:100
    },
    defaults: {
        maxWidth: 300,
    },
    buttonAlign: 'left',
    controller: {
        xclass: 'T.view.hello.Controller'
    },
    reference: 'registrationForm',

    items: [{
        xtype: 'container',
        flex: 1,
        layout: 'anchor',
        items: [{
            xtype: 'displayfield',
            fieldLabel: 'Client\'s image',
            labelAlign: 'top',
            name: 'image',
            config : {tpl: '<img src="data:image/jpeg;base64,{image}" />'},
            updateRecord: function(record) {
                if (record) this.setData(record.data);
            },
            allowBlank: false,
        }]},{
        margin: '0 0 0 70',
    },{

        xtype: 'container',
        flex: 1,
        layout: 'anchor',
        items: [{
            xtype: 'displayfield',
            fieldLabel: 'First Name',
            name: 'firstName',
            allowBlank: false,
        }, {
            xtype: 'displayfield',
            fieldLabel: 'Last Name',
            name: 'lastName',
            allowBlank: false,
        }]
    }, {
        xtype: 'container',
        flex: 1,
        layout: 'anchor',
        margin: '0 0 0 70',
        items: [{
            xtype: 'displayfield',
            fieldLabel: 'personal number',
            name: 'personalNumber',
            allowBlank: false,
        }, {
            xtype: 'displayfield',
            fieldLabel: 'dob',
            name: 'dob',
            allowBlank: false,
            dataIndex: 'date',
            renderer: Ext.util.Format.dateRenderer('Y-m-d H:m:s')
        }]
    }, {
        xtype: 'container',
        flex: 1,
        layout: 'anchor',
        margin: '0 0 0 70',
        items: [{
            xtype: 'displayfield',
            fieldLabel: 'Address',
            name: 'address',
            allowBlank: false,
        }]
    }],
});




