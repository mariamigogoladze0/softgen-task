Ext.define('T.view.hello.Form', {
    extend: 'Ext.form.Panel',
    layout: 'hbox',
    border: false,
    bodyPadding: 5,
    fieldDefaults: {
        anchor: '100%'
    },
    defaults: {
        maxWidth: 300
    },
    buttonAlign: 'left',

    items: [{
        xtype: 'container',
        flex: 1,
        layout: 'anchor',
        items: [{
            xtype: 'textfield',
            fieldLabel: 'First Name',
            labelAlign: 'top',
            name: 'firstName',
        }, {
            xtype: 'textfield',
            fieldLabel: 'Last Name',
            labelAlign: 'top',
            name: 'lastName'
        }]
    }, {
        xtype: 'container',
        flex: 1,
        layout: 'anchor',
        margin: '0 0 0 10',
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Personal Number',
            labelAlign: 'top',
            name: 'personalNumber',
        }, {
            xtype: 'datefield',
            fieldLabel: 'DOB (from)',
            format: 'Y-m-d',
            //labelAlign: 'top',
            name: 'dateFrom'
        },{
            xtype: 'datefield',
            fieldLabel: 'DOB (to)',
            format: 'Y-m-d',
            //labelAlign: 'top',
            name: 'dateTo'
        },]
    }, {
        xtype: 'container',
        flex: 1,
        layout: 'anchor',
        margin: '0 0 0 10',
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Address',
            labelAlign: 'top',
            name: 'address',
        }]
    },],

    buttons: [
        {
            iconCls: 'fa fa-search',
            text: 'ძებნა',
            handler: 'search',
        }, {
            iconCls: 'fa fa-eraser',
            text: 'გასუფთავება',
            handler: 'clean'
        },]
});




