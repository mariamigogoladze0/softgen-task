Ext.define('T.view.hello.Controller', {
    extend: 'Ext.app.ViewController',

    search: function () {
        // let params = {
        //     dob: this.lookup('grid').getValue(),
        //     dateFrom: this.lookup('form').getValue(),
        //     dateTo: this.lookup('form').getValue(),
        // };
        var searchForm = this.lookup('form');
        this.lookup('grid').getStore().load({
            params: searchForm.getValues(),

            url: 'client/filter',

        })
        //this.lookup('form').reset();
    },

    delete: function () {
        let me = this;
        let rec = me.lookup('grid').getSelection()['0'];
        if (!rec) return;
        Ext.Msg.confirm('გაფრთხილება!', 'დაადასტურეთ წაშლა', function f(ans) {
            if (ans === 'yes') {
                me.getView().setLoading();
                rec.erase({
                    //url: 'client/delete/',
                    callback: function () {
                        me.getView().setLoading(false);

                    },
                    success: function () {
                        me.lookup('grid').getStore().reload();

                    },

                });
            }

        });
    },

    clean: function () {
        this.lookup('form').reset();
        this.lookup('grid').getStore().load()
    },

    add: function () {
        const me = this;
        const win = Ext.create('T.view.hello.Window', {
            title: ('კლიენტის დამატება'),
            store: me.lookup("grid").getStore()
        });
        const model = Ext.create('T.model.hello.Client')
        model.setId = null;
        win.lookup('window').loadRecord(model);
        win.show();
    },


    loadClient: function (btn){
        const me = this;
        const grid = me.lookup('grid');
        const rec = grid.getSelectionModel().getSelection()[0];
        if (!rec) return;
        const form = Ext.create('T.view.hello.RegistrationForm', {
            store: grid.getStore(),
            record: rec,
            animateTarget: btn,
            title: 'კლიენტის რეგისტრაცია',
            iconCls: 'fa fa-user-plus',
            closable: true,
        });
        const tabPanel = me.getView();
        tabPanel.add(form);
        tabPanel.setActiveTab(form)
        form.loadRecord(rec)
    },

    edit: function (btn) {
        let me = this;
        let grid = me.lookup('grid');
        let rec = grid.getSelectionModel().getSelection()[0];
        console.log(rec)
        if (!rec) return;
        const win = Ext.create('T.view.hello.Window', {
            store: me.lookup("grid").getStore(),
            record: rec,
            animateTarget: btn,
            title: ('კლიენტის რედაქტირება'),

        });
        win.lookup('window').loadRecord(rec);
        win.show();

    },

    save: function () {
        const me = this;
        const form = me.lookup('window');
        if (!form.isValid()) {
            return;
        }
        form.updateRecord();
        const rec = form.getRecord();
        form.submit({
            url: '/client',
            callback: function (){
                Ext.Msg.alert('სტატუსი', 'მონაცემები წარმატებით შეინახა');

                me.getView().store.reload();
                me.getView().close();
            }
        });

        form.close()
    },

})