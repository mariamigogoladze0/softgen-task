Ext.define('T.view.hello.Grid', {
    extend: 'Ext.grid.Panel',
    title: 'Clients',

    store: {
        xclass: 'T.store.hello.Clients',
        autoLoad: true
    },

    tbar: [{
        text: 'კლიენტის დამატება',
        iconCls: 'fa fa-user-plus',
        handler: 'add'

    }, {
        text: 'წაშლა',
        iconCls: 'fa fa-minus',
        handler: 'delete'
    }, {
        xtype: 'button',
        iconCls: 'fa fa-pencil',
        text: 'რედაქტირება',
        handler: 'edit',
    }],

    columns: [{
        header: 'First Name',
        dataIndex: 'firstName',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        },
        flex: 1
    }, {
        header: 'Last Name',
        dataIndex: 'lastName',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        },
        flex: 1
    }, {
        header: 'Personal Number',
        dataIndex: 'personalNumber',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        },
        flex: 1
    }, {
        xtype: 'datecolumn',
        format: 'Y-m-d H:m:s',
        header: 'DOB',
        dataIndex: 'dob',
        editor: {
            xtype: 'datefield',
            //allowBlank: false
        },
        flex: 1
    }, {
        header: 'Address',
        dataIndex: 'address',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        },
        flex: 1,
    }, {
        header: 'Image',
        dataIndex: 'image',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        },
        flex: 1,
    }],

    listeners: {
        itemdblclick: 'loadClient'
    },

    height: 300,
    width: 400,
})