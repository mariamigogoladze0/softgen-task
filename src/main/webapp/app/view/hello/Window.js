Ext.define('T.view.hello.Window', {
    extend: 'Ext.window.Window',

    width: 400,
    border: false,
    bodyPadding: 5,
    modal: true,
    fieldDefaults: {
        anchor: '100%'
    },
    controller: {
        xclass: 'T.view.hello.Controller'
    },

    items: [{
        xtype: 'form',
        reference: 'window',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        border: false,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'First Name',
            labelAlign: 'top',
            name: 'firstName',
            allowBlank: false,
        }, {
            xtype: 'textfield',
            fieldLabel: 'Last Name',
            labelAlign: 'top',
            name: 'lastName',
            allowBlank: false
        }, {
            xtype: 'container',
            flex: 1,
            layout: 'anchor',
            margin: '0 0 0 10',
        }, {
            xtype: 'textfield',
            fieldLabel: 'personal number',
            labelAlign: 'top',
            name: 'personalNumber',
            allowBlank: false
        }, {
            xtype: 'datefield',
            fieldLabel: 'dob',
            labelAlign: 'top',
            name: 'dob'
        }, {
            xtype: 'container',
            flex: 1,
            layout: 'anchor',
            margin: '0 0 0 10',
        }, {
            xtype: 'textfield',
            fieldLabel: 'Address',
            labelAlign: 'top',
            name: 'address',
            allowBlank: false

        }, {
            xtype: 'container',
            flex: 1,
            layout: 'anchor',
            margin: '0 0 0 10',
        },
            {
                xtype: 'filefield',
                name: 'image',
                fieldLabel: 'Image field',
                labelAlign: 'top',
                layout: 'anchor',
                buttonText: 'Image File...',
                handler: 'imageButton'
            }
        ]
    }],
    buttons: [{
        text: 'შენახვა',
        iconCls: 'fa fa-floppy-o',
        handler: 'save',
    },],

})