package com.example.softgentask.controller;

import com.example.softgentask.model.Client;
import com.example.softgentask.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/client")
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @GetMapping
    public List<Client> getClients() {
        return clientService.getClient();
    }

    @PostMapping
    public ResponseEntity<Client> saveClient(@RequestParam(value = "firstName") String firstName,
                                             @RequestParam(value = "lastName") String lastName,
                                             @RequestParam(value = "personalNumber") String personalNumber,
                                             @RequestParam(value = "dob") Date dob,
                                             @RequestParam(value = "address") String address,
                                             @RequestParam MultipartFile image) throws IOException {
        return new ResponseEntity<>(clientService.saveClient(firstName,lastName,personalNumber,dob,address,image), HttpStatus.CREATED);
    }

    @GetMapping(path = "/filter")
    public List<Client> getFilteredClients(@RequestParam(value = "firstName", required = false) String firstName,
                                           @RequestParam(value = "lastName", required = false) String lastName,
                                           @RequestParam(value = "personalNumber", required = false) String personalNumber,
                                           @RequestParam(value = "dob", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dob,
                                           @RequestParam(value = "dateFrom", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFrom,
                                           @RequestParam(value = "dateTo", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateTo
    ) {
        return clientService.getFilteredClients(firstName, lastName, personalNumber, dob, dateFrom, dateTo);
    }

    @DeleteMapping(path = "/{id}")
    public void removeClient(@PathVariable Integer id) {
        clientService.deleteClient(id);
    }

    @PutMapping(path = "/{id}")
    Client updateClient(@Validated @RequestBody Client client, @PathVariable Integer id) {
        client.setId(id);
        return clientService.updateClient(id, client);
    }
}
