package com.example.softgentask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.servlet.annotation.MultipartConfig;

@MultipartConfig
@SpringBootApplication
public class SoftgenTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoftgenTaskApplication.class, args);
    }

}
