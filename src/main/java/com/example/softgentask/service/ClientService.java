package com.example.softgentask.service;

import com.example.softgentask.exception.RecourseNotFoundException;
import com.example.softgentask.model.Client;
import com.example.softgentask.model.Image;
import com.example.softgentask.repository.ClientRepository;
import com.example.softgentask.repository.ImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.Predicate;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ClientService {
    private final ClientRepository clientRepository;
    private final ImageRepository imageRepository;

    public void deleteClient(Integer id) {
        clientRepository.deleteById(id);
    }

    public List<Client> getClient() {
        return (List<Client>) clientRepository.findAll();
    }

    public List<Client> getFilteredClients(String firstName, String lastName, String personalNumber, Date dob, Date dateFrom, Date dateTo) {

        return clientRepository.findAll((root, q, cb) -> {
            Predicate predicate = cb.conjunction();
            if (firstName != null && !firstName.isEmpty()) {
                predicate = cb.and(predicate, cb.like(root.get("firstName"), firstName + "%"));
            }
            if (lastName != null && !lastName.isEmpty()) {
                predicate = cb.and(predicate, cb.like(root.get("lastName"), lastName + "%"));
            }
            if (personalNumber != null && !personalNumber.isEmpty()) {
                predicate = cb.and(predicate, cb.equal(root.get("personalNumber"), personalNumber));
            }
            if (dateFrom != null && dateTo != null) {
                predicate = cb.and(predicate, cb.between(root.get("dob"), dateFrom, dateTo));
            } else if (dateFrom != null) {
                predicate = cb.and(predicate, cb.greaterThanOrEqualTo(root.get("dob"), dateFrom));
            } else if (dateTo != null) {
                predicate = cb.and(predicate, cb.lessThanOrEqualTo(root.get("dob"), dateTo));
            }

            return predicate;
        });
    }

    public Client updateClient(Integer id, Client client) {
        Optional<Client> clientOptional = clientRepository.findById(id);
        if (clientOptional.isPresent() && client.getId() == id) {
            return clientRepository.save(client);
        } else {
            throw new RecourseNotFoundException();
        }
    }

    public Client saveClient(String firstName, String lastName, String personalNumber, Date dob, String address, MultipartFile image) throws IOException {
        Image img = Image.builder()
                .name(image.getOriginalFilename())
                .content(new String(image.getBytes()))
                .build();

        img = imageRepository.save(img);

        Client client = Client.builder()
                .firstName(firstName)
                .lastName(lastName)
                .personalNumber(personalNumber)
                .dob(dob)
                .address(address)
                .image(img)
                .build();

        return clientRepository.save(client);
    }
}
