package com.example.softgentask.repository;

import com.example.softgentask.model.Client;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer >, JpaSpecificationExecutor<Client> {
    void deleteById(Integer id);

    List<Client> findAllByFirstNameOrLastNameOrDobOrPersonalNumber(String firstName, String lastName, Date dbo, String personalNumber);
}
